package Pyramid_Pattern;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

/* STAR PATTERN :    
						     * 
						    * * 
						   * * * 
						  * * * * 
						 * * * * * 

				     
	 
		 */
		
		System.out.println("Enter The No Of Rows Of Star  Pyramid");
		int rows = sc.nextInt();
		int count  = 1 ;
		for(int i = rows;i>0;i--) {
			for(int j = 1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k = 1;k<=count;k++) {
				System.out.print("* ");
			}
			System.out.println();
			count++;
		}
System.out.println("\n=====================================================================================================================");	


/* ReverseNumber Pattern -1  :
 
 							 7 7 7 7 7 7 7 
							  6 6 6 6 6 6 
							   5 5 5 5 5 
							    4 4 4 4 
							     3 3 3 
							      2 2 
							       1 

   
  */		
		System.out.println("Enter The No Of Rows of Nunber Pattern - 1 :");
		int row1 = sc.nextInt();
		int count1  = row1 ;
		for(int i = 1 ; i<=row1;i++) {
			for(int j = 1 ; j<=i;j++) {
				System.out.print(" ");
			}
			for(int k = count1 ; k>0;k--) {
				System.out.print(count1+" ");
			}
			System.out.println();
			count1--;
		}
System.out.println("\n=====================================================================================================================");	

/* ReverseNumber Pattern -2  :
								 5 4 3 2 1 
								  4 3 2 1 
								   3 2 1 
								    2 1 
								     1 

*/		
System.out.println("Enter The No Of Rows of Nunber Pattern - 2 :");
int row2 = sc.nextInt();
int count2  = row2 ;
for(int i = 1 ; i<=row2;i++) {
for(int j = 1 ; j<=i;j++) {
System.out.print(" ");
}
for(int k = count2 ; k>0;k--) {
System.out.print(k+" ");
}
System.out.println();
count2--;
}

System.out.println("\n=====================================================================================================================");	
		
/*PYRAMID PATTERN :

						   * 
					      * * 
					     * * * 
					    * * * * 
					   * * * * * 
					  * * * * * * 
					 * * * * * * * 

*/
System.out.println("Enter The No Of Rows Of The Pyramid  pattern - 3:");
int row3 = sc.nextInt();
int count3 = 1;
for(int i = row3; i>0; i--) {
	for(int j = 1 ; j<=i;j++) {
		System.out.print(" ");
		
	}
	for(int k = 1;k<=count3;k++) {
		System.out.print("* ");
	}
	System.out.println();
	count3++;
}

System.out.println("\n=====================================================================================================================");	

System.out.println("Enter The No Of Rows Of Pyramid pattern - 4 :");
int row4 = sc.nextInt();
int count4 = row4;
for(int i = 1 ;i<=row4;i++) {
	for(int j = 1 ; j<=i;j++) {
		System.out.print(" ");
	}
	for(int k = count4;k>0;k--) {
		System.out.print("* ");
	}
	
	
	System.out.println();
	count4--;
}









	}
}
