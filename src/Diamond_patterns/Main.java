package Diamond_patterns;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		
/*     PATTERN -1 :
 NOTE : REMEMBER THE TRICKY LOGIC INSIDE THE INNER  FORLOOP 
 	==> if i =5, j<=i , here, j will gona iterate 5 times and then after coming out of, the j loop * j become 1 again*
 	==> if i =4, j<=i , here, j will gona iterate 4 times and then after coming out of, the j loop * j become 1 again*
 	.
 	.
 	.
 	. vice versa til i=0;

 
 								   1 
							      1 2 
							     1 2 3 
							    1 2 3 4 
							   1 2 3 4 5 
							  1 2 3 4 5 6 
							 1 2 3 4 5 6 7 
							 7 6 5 4 3 2 1 
							  6 5 4 3 2 1 
							   5 4 3 2 1 
							    4 3 2 1 
							     3 2 1 
							      2 1 
							       1 

 */
		Scanner sc  = new Scanner(System.in);
		System.out.println("Enter The SIze O the Rows In Diamond Pattern :");
		int row1 = sc.nextInt();
		int count1 = 1 ;
		int count2 = row1;
		for(int i = row1;i>0;i--) {
			for(int j = 1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k = 1;k<=count1;k++) {
				System.out.print(k+" ");
			}
			System.out.println();
			count1++;
		}		
		for(int i = 1;i<=row1;i++) {
			for(int j = 1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k = count2;k>0;k--) {
				System.out.print(k+" ");
			}
			System.out.println();
			count2--;
		}
		
System.out.println("\n============================================================================================");
/*			pattern -2 :
 							 1 
						    2 2 
						   3 3 3 
						  4 4 4 4 
						 5 5 5 5 5 
						  4 4 4 4 
						   3 3 3 
						    2 2 
						     1 
 	
 
 */
		System.out.println("ENter The No Of Rows For Star DIamond Pattern :");
		int row2 = sc.nextInt();
		int scount = 1;
		int scount2 = row2;
		for(int i = row2;i>0;i--) {
			for(int j = 1;j<=i;j++) {
				System.out.print(" ");
				 				 			}
			for(int k = 1;k<=scount;k++) {
				System.out.print(scount+" ");
			}
			System.out.println();
			scount++;

		}
		
		for(int i =1;i<=row2;i++) {
			for(int j = 1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k = scount2;k>0;k--) {
				System.out.print(scount2+" ");
			}
			System.out.println();
			scount2--;
		}
System.out.println("\n============================================================================================");

/*
 	PATTERN - 3:
 						 * 
					    * * 
					   * * * 
					  * * * * 
					 * * * * * 
					 * * * * * 
					  * * * * 
					   * * * 
					    * * 
					     * 
 
 
 */
System.out.println("ENter The No Of Rows For Star DIamond Pattern :");
int row3 = sc.nextInt();
int scount3 = 1;
int scount4 = row3;
for(int i = row3;i>0;i--) {
	for(int j = 1;j<=i;j++) {
		System.out.print(" ");
		 				 			}
	for(int k = 1;k<=scount3;k++) {
		System.out.print("* ");
	}
	System.out.println();
	scount3++;

}

for(int i =1;i<=row2;i++) {
	for(int j = 1;j<=i;j++) {
		System.out.print(" ");
	}
	for(int k = scount4;k>0;k--) {
		System.out.print("* ");
	}
	System.out.println();
	scount4--;
}

	}
	

}

