package Pascal_Trianagles;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Size Of The Pattern : ");
		int size = sc.nextInt();
		
/* 
 		PATTERN-1 :			1 
							1 2 
							1 2 3 
							1 2 3 4 
							1 2 3 4 5 
							1 2 3 4 5 6 
							1 2 3 4 5 6 7 
							1 2 3 4 5 6 
							1 2 3 4 5 
							1 2 3 4 
							1 2 3 
							1 2 
							1 
 */
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		for (int i = size - 1; i >= 0; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		
/*     *
      **
     ***
    ****
   *****
  ******
 *******
  ******
   *****
    ****
     ***
      **
       *    */		
		
		
	System.out.println("Enter The Size For Left ANgled Pascal Triangle :");	
	System.out.println("Enter The No Of Rows :");
	int size1 = sc.nextInt();
	int count = 1;
		for(int i =size1;i>0;i--) {
			for(int j = 1;j<=i;j++) {
				System.out.print(" ");
			}
			for(int k = 1;k<=count;k++) {
				System.out.print("");
				System.out.print(k);
			
			}
			System.out.println();
			count++;
			
		}
		
	
		int count2  = size1-1 ;
		for(int i = 1 ; i<=size1;i++) {
		for(int j = 0 ; j<=i;j++) {
		System.out.print(" ");
		}
		for(int k = count2 ; k>0;k--) {
		System.out.print(k);
		}
		System.out.println();
		count2--;
		}	
		


	}
}
